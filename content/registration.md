+++
title = "Participation"
description = "Volunteering, Speaking and Attending"
keywords = ["tickets","call for papers","volunteer","organiser"]
id = "register"
+++

<br>
### <i class="fas fa-chalkboard-teacher"></i> Call for Tutorial Proposals
The call for proposals is now [live on our website](/news/2019/09/28/call-for-tutorials/)and we have [begun accepting proposals](/news/2019/10/23/tutorial-submission-open/)! The submission portal will close on December 2nd. We plan to notify tutorial applicants of acceptance in early January, prior to the opening of registration.

<br>
### <i class="fas fa-comment"></i> Call for Papers
We anticipate the call for papers being posted on November 15th, 2019. The deadline for abstracts will be February 3rd, 2020. We will open the portal for accepting abstracts when the call for papers is posted, and announce acceptances on March 16th, 2020.

<br>
### <i class="fas fa-user-plus"></i> Registration
We anticipate registration opening on January 15th, 2020, though we will announce pricing this fall. Diversity Scholarship applicants should wait to register until after the announcement of DS awards (around Feb 15) because registration fees will be waived for awardees.

<br>
### <i class="fas fa-users"></i> Diversity Scholarships
As with past useR! conferences, we will be accepting applications for diversity scholarships, which will partially subsidize travel expenses and provide a full registration ticket (including two tutorials and the gala dinner events). There will also be a dedicated networking event for diversity scholarship recipients with the conference's sponsors.The application submission page is open now. More application information can be found [in the original announcement](https://user2020.r-project.org/news/2019/11/18/call-for-diversity-applications/). The deadline for applications is January 15th, 2020, and we will announce recipients the week of February 15th, 2020.

<br>
### <i class="fa fa-glass"></i> R-Ladies Social Event
[R-ladies] (https://rladies.org/) is a worldwide organization whose mission is to promote gender diversity in the R community, in response to the underrepresentation of minority genders (including but not limited to cis/trans women, trans men, non-binary, genderqueer, agender). If you identify as one of these minorities, please join us for an R-Ladies reception during Thursday night of useR! 2020. Come interact with other R-minority users, meet some of the R-Ladies global team, hear about existing chapters and events close to your hometown, or learn how to set up your local R-ladies chapter.

<br>
### <i class="fa fa-graduation-cap" aria-hidden="true"></i> Mentoring 
Attendees looking to connect with and learn from an established R user in their field have the opportunity to be matched with a mentor at the useR! 2020 conference. Any attendee is welcome to sign up to be a mentee or a mentor when registering for the conference. Mentor/mentee pairs will be announced prior to the conference via email and will be formally introduced during a meeting the morning of the first day of the conference (Wednesday, July 8th, 2020). This opportunity provides participants the chance to learn from their peers and form professional connections. Mentees will be given a mentor on a first-come, first-served basis depending on the number of mentors who sign up. 

