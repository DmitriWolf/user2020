+++
title = "Call for Papers"
date = "2019-11-20T09:00:00"
tags = ["Abstracts"]
categories = ["Abstracts"]
banner = "news/papers.jpg"
+++

[The submission portal is now open!](https://slu.secure-platform.com/a/solicitations/38/home)

useR!2020 invites researchers from around the world to submit abstracts for presentation at our 2020 meeting in St. Louis, Missouri, USA. The meeting will feature a wide range of exciting talks, discussions, and networking opportunities.

This is an opportunity to share your work with international colleagues who are R enthusiasts, including many current and future leaders in the R community. We welcome abstracts related to R from any disciplinary background, whether the work is computational, empirical, or theoretical in nature. Investigators at all career stages are encouraged to submit one or more abstracts.

Abstracts will be peer reviewed. Acceptance will be based on content, available space, and overall program balance. At least one author of each accepted abstract will be expected to attend and give a presentation July 8-10, 2020. At most one abstract will be accepted per presenter, with the possible exception of people presenting on behalf of a community organization as well as presenting their own work. The deadline for abstract submission is February 3rd, 2020. All abstracts must be primarily in English, because presentations will be in English.

## R-Ladies Abstract Mentoring
We are partnering with R-Ladies to support submissions from members of underrepresented genders in the R community (including but not limited cis/trans women, trans men, non-binary, genderqueer, agender). The [R-Ladies abstract mentoring system](https://docs.google.com/forms/d/e/1FAIpQLSck8FBjNWjziI8pPIoCBlf5J4oQ_6pzeUdbQ1HjYfJ2bNDwDw/viewform) can be used to get feedback and support on your abstract *prior* to submitting it officially to useR! 2020. We ask that abstracts be submitted to this site no later than January 4, 2020 in order to facilitate feedback in a timely fashion before the useR! 2020 abstract submission deadline.

If you would like to volunteer to be a **mentor** for useR! 2020 abstract submissions, please let us know by filling out [this form](https://docs.google.com/forms/d/e/1FAIpQLSfympgYEYvgZqLQryGcCoktle9GEkAkvHd1ifwoBh_WICAGMA/viewform).

## Oral Presentations
Oral presentations will be 20 minutes. Please consider the following guidelines when submitting an abstract for an oral presentation.

1. Talks should be new to useR!2020. If a talk was presented at a previous useR! conference or at a major conference in North America, then we will judge the abstract on the added contribution since the previous talk.
2. Talks should be originating from the presenter. Talks discussing other people's packages may be considered for lightning talks or posters, but not generally for oral presentations.
3. Most, if not all, successful submissions for oral presentations will have a working and active repo (if a package), a technical report that is available at the time of review, or an available preprint or accepted paper at the time of review. If this does not apply to your work, please consider submitting a lightning talk or poster.
4. Novel case studies are allowed, but typically will be funneled toward lightning talks or posters.
5. Work should be directly related to R. General data science talks are not typically appropriate for oral presentations, though some exceptions may be made.

## Lightning Talks and Posters

Lightning talks are 5 minutes, and are a great way to advertise your work! Or, meet and greet people interested in your work at a poster session. This is where anything related to R that may not quite fit in the oral presentation category is fair game. We would love to see creative and interesting ideas for these.  

## Submission Process
Abstracts can be submitted for consideration soon.  A complete abstract submission requires:

1. A title
2. An abstract of up to 1200 characters.
3. A list of authors with affiliations.
4. A selection of primary area --- see list below for areas.
5. 2-3 keywords.
6. For regular talks, we strongly encourage a link to a git repo, a technical report, a preprint or an accepted paper.
7. For lightning talks and posters, a truly optional link to work by the presenter related to their topic.

## Technical Requirements  
The submission process requires:

1. A title (10 words)
2. An abstract (1200 characters)
3. A list of authors with affiliations.
4. Type of presentation (regular talk/lightning talk/poster).
5. A selection of primary area. See list below.
6. 2-3 keywords that are different from the primary area.
7. An indication whether or not you talked on this topic in North America or at a previous useR! conference. If so, you will be given an opportunity to explain what is new for this talk.

Optional elements:

1. If speaking about a new package that you have developed, a link to a git repository that contains the package code.
2. A link to a paper that is in preprint or accepted form, or to a technical report.
3. If a lightning talk or poster, an optional link that provides evidence of the quality of the current work.

## List of topics from which presenters can choose

1. algorithms
2. applications
3. big/high dimensional data
4. bioinformatics
5. biostatistics/epidemiology
6. community/education
7. data mining
8. databases/data management
9. economics/finance/insurance
10. models
11. multivariate analysis
12. other
13. performance
14. programming
15. R in production
16. reproducibility
17. spatial
18. statistics in social sciences
19. time series
20. visualisation
21. web app
