baseurl = "https://user2020.r-project.org/"
title = "useR 2020"
theme = ["hugo-stl-extras", "hugo-user-theme", "hugo-universal-theme"]
languageCode = "en-us"
# Site language. Available translations in the theme's `/i18n` directory.
defaultContentLanguage = "en"
# Enable comments by entering your Disqus shortname
# disqusShortname = "devcows"
# Enable Google Analytics by entering your tracking code
# googleAnalytics = ""

# address blogdown warning
ignoreFiles = ["\\.Rmd$", "\\.Rmarkdown$", "_files$", "_cache$"]

# Define the number of posts per page
paginate = 10

[menu]

# Main menu
# Comment out sections to prevent them appearing on the nav bar
[[menu.main]]
    name = "Home"
    url  = "/"
    weight = 1

# [[menu.main]]
#   name = "Program"
#   identifier = "program"
#   url  = "/program/"
#   weight = 2

[[menu.main]]
   name = "About"
   url  = "/about/"
   weight = 3

[[menu.main]]
   name = "Participate"
   url  = "/registration/"
   weight = 4

[[menu.main]]
    name = "Venue"
    url  = "/venue/"
    weight = 5

[[menu.main]]
    name = "News"
    url  = "/news/"
    weight = 6

[[menu.main]]
    name = "Code of Conduct"
    url  = "/codeofconduct/"
    weight = 7

[[menu.main]]
    name = "Contact"
    url  = "/contact/"
    weight = 8


# Top bar social links menu

[[menu.topbar]]
    weight = 1
    name = "Twitter"
    url = "https://twitter.com/useR2020stl"
    pre = "<i class='fab fa-2x fa-twitter'></i>"

[[menu.topbar]]
    weight = 2
    name = "GitLab"
    url = "https://gitlab.com/R-conferences/user2020"
    pre = "<i class='fab fa-2x fa-gitlab'></i>"

[[menu.topbar]]
    weight = 3
    name = "Email"
    url = "/contact"
    pre = "<i class='fa fa-2x fa-envelope'></i>"

[params]
    viewMorePostLink = "/news/"
    author = "Jeramia"
    defaultKeywords = ["user2020", "rlang"]
    #defaultDescription = "Site template made by devcows using hugo"

    # Google Maps API key (if not set will default to not passing a key.)
    # googleMapsApiKey = "AIzaSyCFhtWLJcE30xOAjcbSFi-0fnoVmQZPb1Y"

    # Style options: default (light-blue), blue, green, marsala, pink, red, turquoise, violet
    style = "default"

    # Since this template is static, the contact form uses www.formspree.io as a
    # proxy. The form makes a POST request to their servers to send the actual
    # email. Visitors can send up to a 1000 emails each month for free.
    #
    # What you need to do for the setup?
    #
    # - set your email address under 'email' below
    # - upload the generated site to your server
    # - send a dummy email yourself to confirm your account
    # - click the confirm link in the email from www.formspree.io
    # - you're done. Happy mailing!
    #
    # Enable the contact form by entering your Formspree.io email
    email = "useR2020@slu.edu"

    about_us = """<p><strong>
    @useR2020stl // #useR2020
    <br>See something wrong? <a href="https://gitlab.com/R-conferences/user2020">Submit a PR </a>on GitLab
    </p>
    """
    copyright = "Copyright (c) 2019, Saint Louis University; all rights reserved."

    # Format dates with Go's time formatting
    date_format = "January 2, 2006"

    logo = "img/userlogo-small.png"
    logo_small = "img/userlogo-small.png"
    address = """<p><strong>useR! 2020 Organizing Team</strong>
        <br>1900 Morrissey Hall
        <br>Saint Louis University
        <br>3700 Lindell Blvd.
        <br>St. Louis, MO 63108
        <br>
        <strong>United States of America</strong>
      </p>
      """
    latitude = "-12.043333"
    longitude = "-77.028333"

[Permalinks]
    news = "/news/:year/:month/:day/:filename/"

# Enable or disable top bar with social icons
[params.topbar]
    enable = false
    text = """<p class="hidden-sm hidden-xs">Contact us on +420 777 555 333 or hello@universal.com.</p>
      <p class="hidden-md hidden-lg"><a href="#" data-animate-hover="pulse"><i class="fa fa-phone"></i></a>
      <a href="#" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
      </p>
      """

# Enable and disable widgets for the right sidebar
[params.widgets]
    categories = true
    tags = true
    search = true
    contact_map = false

# Hero section
  [params.hero]
    # To change the background image of the hero, replace 'header-bg.jpg' in
    # the 'static/img' folder.
    title = "useR! 2020<br/>The R User Conference<br/>July 7-10, 2020<br/>St. Louis, MO, USA"
    showButton = false
    buttonText = "Tell me more"

[params.carousel]
    enable = false
    # All carousel items are defined in their own files. You can find example items
    # at 'exampleSite/data/carousel'.
    # For more informtion take a look at the README.

[params.features]
    enable = false
    # All features are defined in their own files. You can find example items
    # at 'exampleSite/data/features'.
    # For more informtion take a look at the README.

[params.testimonials]
    enable = false
    # All testimonials are defined in their own files. You can find example items
    # at 'exampleSite/data/testimonials'.
    # For more informtion take a look at the README.
    title = "Testimonials"
    subtitle = "We have worked with many clients and we always like to hear they come out from the cooperation happy and satisfied. Have a look what our clients said about us."

[params.see_more]
    enable = false
    icon = "fa fa-file-code-o"
    title = "Do you want to see more?"
    subtitle = "We have prepared for you more than 40 different HTML pages, including 5 variations of homepage."
    link_url = "#"
    link_text = "Check other homepages"

[params.twitter]
    enable = true
    bg = false
    pattern = true
    photo1 = false
    photo2 = false
    title = "Twitter"
    feed_url = "https://twitter.com/user2020stl"

[params.contact]
    # Since this template is static, the contact form uses Netlify to capture the
    # contact submission. The form makes a POST request to Netlify and you can set up
    # an email notification from Netlify. You can have up to 100 submissions a form a month for free.
    #
    # What you need to do for the setup?
    #
    # You don't need to do anything for Netlify to capture the submissions.
    # In Netlify, go to the "Forms" section and click the "Settings and usage" button.
    # Scroll down to see the "Form Notifications" section, and in "Outgoing Notifications" click the
    # "Add notification" button and click the "Email notification" option.
    # In the modal that pops up, add the official conference email address useRXXX@r-project.org.
    # In the "Form" option, select "Any form". That should be all the setup required.
    subject = [
      "Sponsorship",
      "Registration",
      "Abstract Submission",
      "Tutorials",
      "Schedule",
      "Facilities",
      "Other"
    ]
    recaptcha = true

[params.sponsors]
    enable = true
    bg = false
    pattern = true
    photo1 = false
    photo2 = false
    # All clients are defined in their own files. You can find example items
    # at 'exampleSite/data/clients'.
    # For more informtion take a look at the README.
    title = "Our Sponsors"
    tier1 = "Diamond"
    tier2 = "Platinum"
    tier3 = "Gold"
    tier4 = "Silver"
    tier5 = "Bronze"
    tier6 = "Violet"

[params.recent_posts]
    enable = true
    title = "News"
    subtitle = ""

[params.participation]
    tickets_enable = false
    tickets_link = "http://www.tickets.com"
    tickets_text = "Get your ticket"
    cfp_enable = false
    cfp_link = "http://www.callforpapers.com"
    cfp_text = "Submit your paper"

[params.organising]
    enable = false
    # Ths will be a different page
    bg = true
    pattern= false
    photo1 = false
    photo2 = false
    # Possibility to center items
    center = true
    title = "Team"
    subtitle = "Organizing Commitee"
    description = "Local team organizing the event"

    # All organising member's pictures are stored under 'static/img/organising'.
    # Replace them with your own too.
    # Suggestion: Image size should be at least 360px*360px or the alignment might be affected.
    [[params.organising.members]]
      img = "faceholder.png"
      name = "Organizer Name 1"
      position = "Organizer Position 1"
      reportsTo = "Some Person"
      livesIn = "[Munich, Germany](#some-maps-url)"
      scope = [
        "Role 1 for [thing 1](#)",
        "Role 2 for [thing 2](#)",
        "Role 3 for [thing 3](#)"
      ]
      # For bio markdown and even multiline strings are available.
      bio = """I do things,
      and stuff"""
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.organising.members]]
      img = "faceholder.png"
      name = "Organizer Name 2"
      position = "Organizer Position 2"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.organising.members]]
      img = "faceholder.png"
      name = "Organizer Name 3"
      position = "Organizer Position 3"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.organising.members]]
      img = "faceholder.png"
      name = "Organizer Name 4"
      position = "Organizer Position 4"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.organising.members]]
      img = "faceholder.png"
      name = "Organizer Name 5"
      position = "Organizer Position 5"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.organising.members]]
      img = "faceholder.png"
      name = "Organizer Name 6"
      position = "Organizer Position 6"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

[params.programming]
    enable = false
    # Also a different page
    bg = true
    pattern = false
    photo1 = false
    photo2 = false
    # Possibility to center items
    center = true
    # title = ""
    subtitle = "Program committee"
    description = "Program the thing"

    # All programming member's pictures are stored under 'static/img/programming'.
    # Replace them with your own too.
    # Suggestion: Image size should be at least 360px*360px or the alignment might be affected.
    [[params.programming.members]]
      img = "faceholder.png"
      name = "Programmer Name 1"
      position = "Programmer Position 1"
      reportsTo = "Some Person"
      livesIn = "[Munich, Germany](#some-maps-url)"
      scope = [
        "Role 1 for [thing 1](#)",
        "Role 2 for [thing 2](#)",
        "Role 3 for [thing 3](#)"
      ]
      # For bio markdown and even multiline strings are available.
      bio = """I do things,
      and stuff"""
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.programming.members]]
      img = "faceholder.png"
      name = "Programmer Name 2"
      position = "Programmer Position 2"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.programming.members]]
      img = "faceholder.png"
      name = "Programmer Name 3"
      position = "Programmer Position 3"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.programming.members]]
      img = "faceholder.png"
      name = "Programmer Name 4"
      position = "Programmer Position 4"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.programming.members]]
      img = "faceholder.png"
      name = "Programmer Name 5"
      position = "Programmer Position 5"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

    [[params.programming.members]]
      img = "faceholder.png"
      name = "Programmer Name 6"
      position = "Programmer Position 6"
      social = [
        ["fa-twitter", "#"],
        ["fa-github", "#"],
        ["fa-linkedin", "#"],
        ["fa-globe", "#"], # WWW
        ["fa-id-card", "#"] # ORCID
      ]

[params.dates]
    enable = true
    bg = true
    pattern = false
    photo1 = false
    photo2 = false
    title = "Upcoming Deadlines"

[params.about]
    enable = false
    # You can't pass block level markdown to Markdownify"
    bg = true
    pattern = false
    photo1 = false
    photo2 = false
    title = "About"
    text = '''
    # What is the useR! conference?
    useR! is an annual, international conference of leading statisticians and data scientists from around the world.
    In recent years, it has been held in Toulouse, France; Brisbane, Australia; Brussels, Belgium; and Stanford,
    California. The conference is the main annual meeting of the R community, and attendance in 2020 is anticipated
    to be over 1,000 people. Attendees will include R developers and users who are data scientists, business
    intelligence specialists, analysts, statisticians from academia and industry, and students.
    '''
